
package Code;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;


public class GradientBackGround extends JPanel{
	
	    protected void paintComponent(Graphics graphics) {
	        super.paintComponent(graphics);
	        Graphics2D graphics2d = (Graphics2D) graphics;
	        graphics2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	        int width = getWidth();
	        int height = getHeight();
	        Color color1 = Color.blue;
	        Color color2 = Color.white;
	        GradientPaint gp = new GradientPaint(0, 0, color1, 0, height, color2);
	        graphics2d.setPaint(gp);
	        graphics2d.fillRect(0, 0, width, height);
	    }

}
