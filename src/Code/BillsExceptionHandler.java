package Code;

public class BillsExceptionHandler extends Exception {
	
	String message;
	
	public BillsExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage(){
		
		return message;
	}

}
