package Code;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import Handlers.ExceptionHandler;


public class Bills {

	private Connection connection = null;
	private static Statement statement = null;

	private static String toExcute;
	private static ResultSet resultSet = null;

	public Bills() {

	}

	public static double getPublicationsPrice() throws ExceptionHandler {

		double res = 0;
		toExcute = ("SELECT * from newsagent.publication;");

		try {
			resultSet = statement.executeQuery(toExcute);
			System.out.println(toExcute);

			if (!resultSet.next()) {
				System.out.println("Nothing found");

			} else {

				do {
					System.out.print(resultSet.getString("pub_name"));
					System.out.print(" ");
					System.out.println(resultSet.getString("cost"));

				} while (resultSet.next());
			}

		} catch (Exception e) {
			new Exception("Error");
		}

		return res;

	}

	public static double getDeliveryPrice() throws ExceptionHandler {
		double res = 0;
		toExcute = ("SELECT * from newsagent.deliveries;");

		try {
			resultSet = statement.executeQuery(toExcute);
			System.out.println(toExcute);

			if (!resultSet.next()) {
				System.out.println("Nothing found");

			} else {

				do {
					System.out.print(resultSet.getString("del_id"));
					System.out.print(" ");
					System.out.println(resultSet.getString("del_charge"));

				} while (resultSet.next());
			}

		} catch (Exception e) {
			new Exception("Error with query");
		}
		return res;

	}

	public static double getCustomerBalance() throws ExceptionHandler {

		return 0;
	}

	public static double CalculateBill() throws ExceptionHandler {

		return 0;

	}

	public static double setCustomerBalance() throws ExceptionHandler {

		return 0;

	}

	public static void generateCsvFile(String sFileName, int id) {
		try {
			DataBase datbaseInstance = DataBase.getInstance();
			PrintBill printBill = datbaseInstance.dataForFileBill(id);
			FileWriter writer = new FileWriter(sFileName);

			writer.append("Cust_id");
			writer.append(',');
			writer.append("Name");
			writer.append(',');
			writer.append("Balance");
			writer.append('\n');

			writer.append(String.valueOf(printBill.getId()));
			writer.append(',');
			writer.append(printBill.getName());
			writer.append(',');
			writer.append(String.valueOf(printBill.getPrice()));
			writer.append('\n');

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
