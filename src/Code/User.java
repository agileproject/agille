package Code;

import java.util.Scanner;

public class User {
	static Scanner in = new Scanner(System.in);
	static DataBase db = DataBase.getInstance();
	
	private int id;
	private String firstName;
	private String lastName;
	
	public User(String firstName, String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	}
	
