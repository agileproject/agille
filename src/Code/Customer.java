package Code;

import Handlers.ExceptionHandler;
import java.util.Scanner;


public class Customer extends User {
	

	private String street;
	private String town;
	private String location;
	private int houseNumber;
	private String ContactNumber;
	private boolean onBreak;

	private static boolean defaultTrueStatus = true;

	// constructor for customer object
	public Customer(int customerId, String firstName, String lastName,
			String street, String town, String location, int houseNumber,
			String ContactNumber, boolean onBreak) {
		super(firstName, lastName);
		this.street = street;
		this.town = town;
		this.location = location;
		this.houseNumber = houseNumber;
		this.ContactNumber = ContactNumber;
		this.onBreak = onBreak;
	}

	// constructor for customer object (initial constructor before id generated
	// by database tables)
	public Customer(String firstName, String lastName, String street,
			String town, String location, int houseNumber,
			String ContactNumber, boolean onBreak) {
		super(firstName, lastName);
		this.street = street;
		this.town = town;
		this.location = location;
		this.houseNumber = houseNumber;
		this.ContactNumber = ContactNumber;
		this.onBreak = onBreak;
	}

	// public User getUser() {
	// return user;
	// }
	//
	// public void setUser(User user) {
	// this.user = user;
	// }

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}
	
	public String getContactNumber() {
		return ContactNumber;
	}

	public void setContactNumber(String contactNumber) {
		ContactNumber = contactNumber;
	}

	public boolean isOnBreak() {
		return onBreak;
	}

	public void setOnBreak(boolean onBreak) {
		this.onBreak = onBreak;
	}

	// Author Declan Farrell A00195947
	// registerCustomer - main method
	public static boolean registerCustomer(String firstName, String lastName,
			String town, String street, String location, int houseNumber,
			String contactNumber) throws ExceptionHandler {
		boolean resultOfCheck = false;
		DataBase databaseInstance = DataBase.getInstance(); // get instance of
															// database object
		Customer newCustomer = validateRegisterCustomer(firstName, lastName,
				town, street, location, houseNumber, contactNumber); // validate
																		// variables
		if (newCustomer != null) { // execute registerCustomer error handling
			resultOfCheck = databaseInstance
					.tryCatchRegisterCustomer(newCustomer);
			return resultOfCheck;
		} else if (newCustomer == null) {
			throw new ExceptionHandler("registerCustomerError");
		}
		return resultOfCheck;
	}

	// Author Declan Farrell A00195947
	// register Customer - validation of variables
	public static Customer validateRegisterCustomer(String firstName,
			String lastName, String town, String street, String location,
			int houseNumber, String contactNumber) {
		Customer newCustomer = null; // declare customer object
		// validate variables entered and meet all conditions
		if (firstName.length() > 0 && lastName.length() > 0
				&& town.length() > 0 && street.length() > 0
				&& location.length() > 0 && houseNumber >= 0
				&& contactNumber.startsWith("090")
				|| contactNumber.startsWith("08")) { 
			newCustomer = new Customer(firstName, lastName, town, street,
					location, houseNumber, contactNumber, defaultTrueStatus);
		}
		return newCustomer;
	}

	// Author Declan Farrell A00195947
	// viewCustomerDetails
	public static Customer viewCustomerDetails(int customerId)
			throws ExceptionHandler {
		DataBase databaseInstance = DataBase.getInstance(); // get instance of
															// database object
		Customer customerDetails = null; // customer Details object
		if (checkCustomerExists(customerId)) { // check cust_id > 0
			// call viewCustomerDetailsDB method
			customerDetails = databaseInstance
					.tryCatchViewCustomerDetails(customerId); 
		} else if (!checkCustomerExists(customerId)) {
			throw new ExceptionHandler("viewCustomerDetailsError");
		}
		return customerDetails; // return details

	}

	// Declan Farrell A00195947
	// removePublicationsFromDeliveryList
	public static boolean removePublicationFromDeliveryList(int customerId,
			int publicationId) throws ExceptionHandler, Handler.ExceptionHandler {
		// get Instance of database object
		DataBase databaseInstance = DataBase.getInstance(); 
		boolean checkMethodRunsCorrectly = false;
		
		if (checkCustomerExists(customerId)
				&& databaseInstance.searchPublications(publicationId)) { 
			 // execute tryCatchRemovePublicationFromDeliveryList
			checkMethodRunsCorrectly = databaseInstance
					.tryCatchRemovePublicationFromDeliveryList(customerId,
							publicationId);
		} else if (!checkCustomerExists(customerId) // else throw exception
				|| !databaseInstance.searchPublications(publicationId)) { 
			throw new ExceptionHandler(
					"removePublicationsFromDeliveryListError");
		}
		return checkMethodRunsCorrectly;

	}

	public boolean addPublicationsToDeliveryList() {
		throw new RuntimeException();
	}

	// Patrick Murray A00202026
	public boolean updatePersonalInformation(String fName, String lName)
			throws ExceptionHandler, Handler.ExceptionHandler {
		// Database command
		// UPDATE customer SET column1=value1,column2=value2,... WHERE
		// cus_id=customerID;

		DataBase db = DataBase.getInstance();
		boolean res = false;
		StringBuilder command = new StringBuilder();

		if (db.searchCustomer(fName, lName)) {

			Scanner input = new Scanner(System.in);

			command.append("UPDATE customers SET ");

			String values[] = new String[5];
			String colVal[] = { "street='", "town='", "location='",
					"houseNo='", "contactNo='" };

			System.out.println("Street");
			values[0] = input.nextLine() + "'";

			System.out.println("town");
			values[1] = input.nextLine() + "'";

			System.out.println("location");
			values[2] = input.nextLine() + "'";

			System.out.println("house No.");
			values[3] = input.nextLine() + "'";

			System.out.println("contact No.");
			values[4] = input.nextLine() + "'";
			int c = 0;
			boolean comm = false;
			for (String x : values) {
				if (values[c].length() == 1) {
					c++;
					continue;
				} else {
					if (comm == false) {
						command.append(colVal[c] + values[c]);
						comm = true;
						c++;
					} else {
						command.append("," + colVal[c] + values[c]);
						res = true;
						c++;
					}
				}
			}

			// Set text field to id then pull that instead of hardCode
			command.append(" WHERE cust_id='3';");


		if (!res)
			throw new ExceptionHandler("updateError");
		else
			db.updateCustomer(command.toString());

		
		}

		return res;
	}

	// Declan Farrell A00195947
	// suspendService
	public static boolean suspendCustomerService(int customerId)
			throws ExceptionHandler {
		DataBase databaseInstance = DataBase.getInstance(); // get instance of
															// database object
		boolean checkMethodRunsCorrectly = false;
		if (checkCustomerExists(customerId)) { // check customer exsist
			// call suspendCustomerService method
			checkMethodRunsCorrectly = databaseInstance
					.tryCatchSuspendCustomerService(customerId); 
		} else if (!checkCustomerExists(customerId)) { // catch error
			throw new ExceptionHandler("suspendError"); // throw exception
		}
		return checkMethodRunsCorrectly; // return details
	}

	// Declan Farrell A00195947
	// resumeService
	public static boolean resumeCustomerService(int customerId)
			throws ExceptionHandler {
		// get instance of database object
		DataBase databaseInstance = DataBase.getInstance(); 
		boolean checkMethodRunsCorrectly = false;
		if (checkCustomerExists(customerId)) { // check customer exists
			// call resumeCustomerService method
			checkMethodRunsCorrectly = databaseInstance
					.tryCatchResumeCustomerService(customerId); 

		} else if (!checkCustomerExists(customerId)) { // catch error
			throw new ExceptionHandler("resumeError"); // throw exception
		}
		return checkMethodRunsCorrectly; // return details
	}

	// Author Declan Farrell A00195947
	// checkCustomerExists
	public static boolean checkCustomerExists(int customerId)
			throws ExceptionHandler {
		// get instance of database object
		DataBase databaseInstance = DataBase.getInstance(); 
		// execute tryCatchCheckCustomerExists & return result
		if (!databaseInstance.tryCatchCheckCustomerExists(customerId)) { 
			return false;
		} else {
			return true;
		}

	}
	
	public static boolean addPublicationToDeliveryList(int customerId, int publicationId) throws ExceptionHandler {
		DataBase databaseInstance = DataBase.getInstance();
		boolean checkMethodRunsCorrectly = false;
		if(checkCustomerExists(customerId) && publicationId > 0){
			System.out.println("check1");
			checkMethodRunsCorrectly = databaseInstance.tryCatchAddPublicationToDeliveryList(customerId,publicationId);
		}else if (!checkCustomerExists(customerId)|| publicationId <= 0){
			throw new ExceptionHandler("addPublicationToDeliveryListError");
		}
		return checkMethodRunsCorrectly;
		
	}

}
