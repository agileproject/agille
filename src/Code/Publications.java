package Code;

import Handlers.ExceptionHandler;

public class Publications {

	public static boolean addPublicationToDatabase(String publicationName,
			double publicationPrice) throws ExceptionHandler {
		DataBase databaseInstance = DataBase.getInstance();
		boolean checkMethodRunsCorrectly = false;
		if (publicationPrice > 0) {
			checkMethodRunsCorrectly = databaseInstance
					.tryCatchAddPublicationToDatabase(publicationName,
							publicationPrice);
		} else if (publicationPrice < 0) {
			throw new ExceptionHandler("addPublicationToDatabaseError");
		}
		return checkMethodRunsCorrectly;

	}

	public static boolean removePublicationFromDatabase(int publicationId)
			throws ExceptionHandler {
		DataBase databaseInstance = DataBase.getInstance();
		boolean checkMethodRunsCorrectly = false;
		if (publicationId > 0) {
			checkMethodRunsCorrectly = databaseInstance
					.tryCatchRemovePublicationFromDatabase(publicationId);
		} else if (publicationId <= 0) {
			throw new ExceptionHandler("removePublicationFromDatabaseError");
		}
		return checkMethodRunsCorrectly;

	}

	public boolean searchForPublication() {
		throw new RuntimeException();

	}

	public static boolean updatePublication(int publicationId,
			String publicationName, double publicationCost)
			throws ExceptionHandler {
		DataBase databaseInstance = DataBase.getInstance();
		boolean checkMethodRunsCorrectly = false;
		if (publicationId > 0 && publicationCost > 0) {
			System.out.println("check1");
			checkMethodRunsCorrectly = databaseInstance
					.tryCatchUpdatePublicationToDatabase(publicationId,
							publicationName, publicationCost);
		} else if (publicationId <= 0 || publicationCost <= 0) {
			throw new ExceptionHandler("updatePublicationError");
		}
		return checkMethodRunsCorrectly;

	} // comments to do
}
