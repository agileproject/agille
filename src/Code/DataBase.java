package Code;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Handler.ExceptionHandler;

public class DataBase {

	// declaration of variables
	private static DataBase databaseObject;
	private static boolean databaseObjectCreated = false;
	private Connection connection = null;
	private Statement statement = null;
	double total = 0.0;
	int numOfDel = 0;
	ArrayList<String> north = new ArrayList<String>();
	ArrayList<String> south = new ArrayList<String>();
	ArrayList<String> east = new ArrayList<String>();
	ArrayList<String> west = new ArrayList<String>();
	ArrayList<String> northFull = new ArrayList<String>();
	ArrayList<String> southFull = new ArrayList<String>();
	ArrayList<String> eastFull = new ArrayList<String>();
	ArrayList<String> westFull = new ArrayList<String>();

	String northCommand = ("select * from customers where location = 'N';");
	String southCommand = ("select * from customers where location = 'S';");
	String eastCommand = ("select * from customers where location = 'E';");
	String westCommand = ("select * from customers where location = 'W';");

	private String toExecute;
	private ResultSet resultSet = null;

	private boolean defaultTrueStatus = true;
	private int falseStatus = 0;
	private int trueStatus = 1;

	// constructor for Database Object
	private DataBase() {
		databaseObject = this;
		databaseObjectCreated = true;
		connectToDatabase();
	}

	// Singelton Pattern Declan Farrell A00195947
	// get Instance of only database object
	public static DataBase getInstance() {
		if (!databaseObjectCreated) {
			databaseObject = new DataBase();
		}
		return databaseObject;
	}

	public String date() {
		return (new SimpleDateFormat("yyyy-MM-dd").format(Calendar
				.getInstance().getTime()));
	}

	public String time() {
		return (new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()));
	}
	

	public void connectToDatabase() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// connection for Declan's system
			if (System.getProperty("user.name").equals("declan")) {
				connection = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/newsagent", "root", "");
				// connection for Farooq's system
			} else if (System.getProperty("user.name").equals("SKYLEX")) {
				connection = DriverManager
						.getConnection("jdbc:mysql://localhost:3306/newsagent",
								"root", "root");
				// connection for EveryBody else's system
			} else {
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/newsagent", "root", "admin");
							
			}

			statement = connection.createStatement();
		} catch (Exception e) {
			System.out.println("Failed to Connect to the Database");
			e.printStackTrace();
		}
		

	} // end of connectToDatabase()

	public boolean register(String custId, String publication, double pubPrice, double delPrice, double totalPrice ) {

		try {
			String updateRegister = "INSERT INTO register VALUES('" + custId + "','" + publication + "');";

			System.out.println(updateRegister);

			statement.executeUpdate(updateRegister);

		} catch (SQLException sqle) {
			System.err.println("Error with  insert:\n" + sqle.toString());
			return false;
		}
		return true;
	}
	
	public boolean Login(String username, String Password) {

		try {
			String updateTemp = "select * from driver " + "where name= '"
					+ username + "'" + "and password= '" + Password + "'";

			resultSet = statement.executeQuery(updateTemp);

			if (resultSet.next()) {
				System.out.println(resultSet.getInt("id"));
				return true;
			} else
				return false;

		} catch (SQLException sqle) {
			System.err.println("Error with login:\n" + sqle.toString());
			return false;
		}

	}

	public boolean Rigister(String name, String password, String deliveryinfo) {
		try {
			String updateTemp = "INSERT INTO DRIVER VALUES(" + null + ",'"
					+ name + "','" + password + "','" + deliveryinfo + "');";

			statement.executeUpdate(updateTemp);

			return true;

		} catch (SQLException sqle) {
			System.err.println("Error with login:\n" + sqle.toString());
			return false;
		}
	}

	// Patrick Murray A00202026
	// Search database for publications by String
	public boolean searchPublications(String searchFor) throws ExceptionHandler {

		// Command to execute
		// SELECT * from publication WHERE pub_name =searchFor;

		// create boolean & initialize to false;
		boolean res = false;
		// check parameter passed isn't empty
		if (searchFor != null) {
			toExecute = ("SELECT * from publication WHERE pub_name ='"
					+ searchFor + "';");
			try {
				resultSet = statement.executeQuery(toExecute);

				// checks resultset is not null & moves pointer to first row
				if (!resultSet.next()) {
					System.out.println("No such publication in Database");
				} else {
					// loop through while first once because pointer on first
					// row
					do {
						System.out.println(resultSet.getString("pub_name"));
						System.out.println(resultSet.getString("cost"));
						res = true;
					} while (resultSet.next());
				}

			} catch (Exception e) {
				throw new ExceptionHandler("searchError");
			}

		} else {
			res = false;
		}
		return res;
	}

	// Patrick Murray A00202026
	// Search database for publications by ID
	public boolean searchPublications(int pubID) throws ExceptionHandler {

		// Command to execute
		// SELECT * from publication WHERE pub_name =searchFor;

		if (pubID > 0) {
		} else {
			pubID = -1;
		}

		// create boolean & initialize to false;
		boolean res = false;
		// check parameter passed isn't negative number
		if (pubID != -1) {
			toExecute = ("SELECT * from publication WHERE pub_id ='" + pubID + "';");
			try {
				resultSet = statement.executeQuery(toExecute);

				if (!resultSet.next()) {
					System.out.println("No such ID in Database");
				} else {

					do {
						System.out.println(resultSet.getString("pub_name"));
						res = true;
					} while (resultSet.next());
				}

			} catch (Exception e) {
				throw new ExceptionHandler("updateError");
			}

		} else {
			res = false;
		}
		return res;
	}

	// Patrick Murray A00202026
	// Search customers by first & last name
	public boolean searchCustomer(String fName, String lName) throws ExceptionHandler {
		// Command to execute
		// SELECT firstName, lastName from customers WHERE firstName = 'fName'
		// AND lastName = 'lName';

		boolean res = false;

		if (fName != null & lName != null) {
			toExecute = ("SELECT firstName, lastName from customers WHERE firstName = '"
					+ fName + "' AND lastName = '" + lName + "';");

			try {
				resultSet = statement.executeQuery(toExecute);
				System.out.println(toExecute);

				if (!resultSet.next()) {
					System.out.println("No such person in the Database");
					res = false;
				} else {

					do {
						System.out.print(resultSet.getString("firstName"));
						System.out.print(" ");
						System.out.println(resultSet.getString("lastName"));
						res = true;
					} while (resultSet.next());
				}

			} catch (Exception e) {
				throw new ExceptionHandler("searchError");
			}
		} else {
			if (fName == null)
				System.out.println("Please check first name");
			if (lName == null)
				System.out.println("Please check last name");
			res = false;
		}
		return res;

	}
	
	// Patrick Murray A00202026
	// update customer information
	public void updateCustomer(String command) throws ExceptionHandler {

		try {
			PreparedStatement statement = connection.prepareStatement(command);
			statement.executeUpdate();
		} catch (Exception e) {
			throw new ExceptionHandler("updateError");
		}
	}

	// Print daily summaries

	public boolean printDailySummary(String date) throws ExceptionHandler {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		boolean res = false;

		if (date.length() != 10) {
			throw new ExceptionHandler("dateFormatError");
		} else {
			try {
				Date todayDate = sdf.parse(date());
				Date enteredDate = sdf.parse(date);

				if (todayDate.before(enteredDate)) {
					System.out
							.println("Cant search by future date\nPlease check entered date!?!");
					return res;
				} else {
					res = true;
					searchDBbyDate(date);
					System.out.println(numOfDel + " Delivaries\n�" + total
							+ " income");
				}

			} catch (Exception e) {
				throw new ExceptionHandler("dateFormatError");
			}
		}
		return res;
	}

	// Search for deliveries by date
	public void searchDBbyDate(String date) throws ExceptionHandler {

		// SQL command
		toExecute = ("SELECT * from deliveries WHERE del_date = '" + date + "';");

		try {
			resultSet = statement.executeQuery(toExecute);

			if (!resultSet.next()) {
				throw new ExceptionHandler("No deliveries for given date");
			} else {

				do {
					numOfDel++;
					total += resultSet.getDouble("del_charge");
					System.out.println(total);
				} while (resultSet.next());
			}
		} catch (Exception e) {
			throw new ExceptionHandler("dailyError");
		}
	}

	// PAtrick Murray A00202026
	public void groupNorth() throws ExceptionHandler {
		try {
			resultSet = statement.executeQuery(northCommand);

			if (!resultSet.next()) {
				throw new ExceptionHandler("emptyDatabase");
			} else {
				do {
					north.add(resultSet.getString("firstName"));
					north.add(resultSet.getString("location"));
					north.add(resultSet.getString("houseNo"));
				} while (resultSet.next());
			}

			northFull.addAll(north);

		} catch (Exception e) {
			throw new ExceptionHandler("groupErrorNorth");
		}
	}

	// PAtrick Murray A00202026
	public void groupSouth() throws ExceptionHandler {
		try {
			resultSet = statement.executeQuery(southCommand);

			if (!resultSet.next()) {
				throw new ExceptionHandler("emptyDatabase");
			} else {
				do {
					south.add(resultSet.getString("firstName"));
					south.add(resultSet.getString("location"));
					south.add(resultSet.getString("houseNo"));
				} while (resultSet.next());
			}

			southFull.addAll(south);

		} catch (Exception e) {
			throw new ExceptionHandler("groupErrorSouth");
		}
	}

	// PAtrick Murray A00202026
	public void groupEast() throws ExceptionHandler {
		try {
			resultSet = statement.executeQuery(eastCommand);

			if (!resultSet.next()) {
				throw new ExceptionHandler("emptyDatabase");
			} else {
				do {
					east.add(resultSet.getString("firstName"));
					east.add(resultSet.getString("location"));
					east.add(resultSet.getString("houseNo"));
				} while (resultSet.next());
			}

			eastFull.addAll(east);

		} catch (Exception e) {
			throw new ExceptionHandler("groupErrorEast");
		}
	}

	// PAtrick Murray A00202026
	public void groupWest() throws ExceptionHandler {
		try {
			resultSet = statement.executeQuery(westCommand);

			if (!resultSet.next()) {
				throw new ExceptionHandler("emptyDatabase");
			} else {
				do {
					west.add(resultSet.getString("firstName"));
					west.add(resultSet.getString("location"));
					west.add(resultSet.getString("houseNo"));
				} while (resultSet.next());
			}

			westFull.addAll(west);

		} catch (Exception e) {
			throw new ExceptionHandler("groupErrorWest");
		}
	}

	public void printDelivaryRoute() throws ExceptionHandler {
		groupNorth();
		groupSouth();
		groupEast();
		groupWest();
		System.out.println(westFull);
		System.out.println(southFull);
		System.out.println(northFull);
		System.out.println(westFull);
	}
	// Author Declan Farrell A00195947
	// registerCustomer - error handling
	public boolean tryCatchRegisterCustomer(Customer newCustomer) {
		try {
			ExecuteRegisterCustomer(newCustomer); // run processing method
			return true;
		} catch (Exception error) {
			return false;
		}

	}

	// Author Declan Farrell A00195947
	// registerCustomer - processing
	public void ExecuteRegisterCustomer(Customer newCustomer) throws Exception {
		// build statement for execution
		PreparedStatement preparedstatement = connection
				.prepareStatement("INSERT INTO customers (firstName,lastName,street,town,location,houseNo,contactNo,status) VALUES (?,?,?,?,?,?,?,?)");

		// add values to statement placeholders
		preparedstatement.setString(1, newCustomer.getFirstName());
		preparedstatement.setString(2, newCustomer.getLastName());
		preparedstatement.setString(3, newCustomer.getStreet());
		preparedstatement.setString(4, newCustomer.getTown());
		preparedstatement.setString(5, newCustomer.getLocation());
		preparedstatement.setInt(6, newCustomer.getHouseNumber());
		preparedstatement.setString(7, newCustomer.getContactNumber());
		preparedstatement.setBoolean(8, defaultTrueStatus);

		preparedstatement.executeUpdate(); // execute statement
	}

	// Declan Farrell A00195947
	// viewCustomerDetails - error handling
	public Customer tryCatchViewCustomerDetails(int customerId) {
		try {
			return ExecuteViewCustomerDetails(customerId); // run processing
															// method
		} catch (Exception error) {
			return null;
		}

	}

	// Declan Farrell A00195947
	// viewCustomerDetails - processing
	public Customer ExecuteViewCustomerDetails(int customerId) throws Exception {
		toExecute = "SELECT * from customers where cust_id = " + customerId
				+ ";"; // sql statement
		Customer customerDetails; // declare customer object
		resultSet = statement.executeQuery(toExecute); // execute statement &
														// gather resultSet
		if (!resultSet.next()) { //
			customerDetails = null;
		} else {
			customerDetails = gatherCustomerDetails(customerId, resultSet); // Execute
																			// gatherCustomerDetails
																			// &
																			// return
																			// result
			return customerDetails;
		}
		return customerDetails;
	}

	// Declan Farrell A00195947
	// viewCustomerDetails
	public Customer gatherCustomerDetails(int customerId, ResultSet resultSet)
			throws SQLException {
		// create customer object from resultSet
		Customer customerDetails = new Customer(customerId,
				resultSet.getString("firstName"),
				resultSet.getString("lastName"), resultSet.getString("street"),
				resultSet.getString("town"), resultSet.getString("location"),
				resultSet.getInt("houseNo"), resultSet.getString("contactNo"),
				resultSet.getBoolean("status"));
		return customerDetails; // return object
	}

	// Declan Farrell A00195947
	// suspendCusetomerService - error handling
	public boolean tryCatchSuspendCustomerService(int cust_id) {
		try {
			ExecuteSuspendCustomerService(cust_id); // run processing method
			return true;
		} catch (Exception error) {
			return false;
		}

	}

	// Declan Farrell A00195947
	// suspendCusetomerService -processing
	public void ExecuteSuspendCustomerService(int customerId) throws Exception {
		toExecute = "UPDATE customers Set status = '" + falseStatus
				+ "' where cust_id = '" + customerId + "';"; // sql statement
		statement.executeUpdate(toExecute); // execute sql

	}

	// Declan Farrell A00195947
	// resumeCusetomerService - error handling
	public boolean tryCatchResumeCustomerService(int customerId) {
		try {
			ExecuteResumeCustomerService(customerId); // run processing method
			return true;
		} catch (Exception error) {
			return false;
		}
	}

	// Declan Farrell A00195947
	// resumeCusetomerService - processing
	public void ExecuteResumeCustomerService(int customerId) throws Exception {
		toExecute = "UPDATE customers Set status = '" + trueStatus
				+ "' where cust_id = '" + customerId + "';"; // sql statement
		statement.executeUpdate(toExecute); // execute statement
	}

	// Declan Farrell
	// removePublicationsFromDeliveryList - error handling
	public boolean tryCatchRemovePublicationFromDeliveryList(int customerId,
			int publicationId) {
		try { // run processing method
			ExecuteRomovePublicationFromDeliveryList(customerId, publicationId);
			return true;
		} catch (Exception error) {
			return false;
		}
	}

	// Declan Farrell
	// removePublicationsFromDeliveryList - processing
	private void ExecuteRomovePublicationFromDeliveryList(int customerId,
			int publicationId) throws SQLException {
		toExecute = "DELETE FROM customersPublications WHERE customerId = "
				+ customerId + " and publicationId = " + publicationId; // sql
																		// statement
		statement.executeUpdate(toExecute); // execute statement

	}

	// Declan Farrell A00195947
	// checkCustomerExists - error handling
	public boolean tryCatchCheckCustomerExists(int customerId) {
		try {
			return ExceuteCheckCustomerExists(customerId); // run processing
															// method, return
															// result
		} catch (Exception error) {
			return false;
		}

	}

	// Declan Farrell A00195947
	// checkCustomerExists - processing
	private boolean ExceuteCheckCustomerExists(int customerId) throws Exception {
		toExecute = "Select * from customers where cust_id = " + customerId
				+ ";"; // sql statement
		resultSet = statement.executeQuery(toExecute); // execute statement,
														// gather result set
		if (!resultSet.next()) { // check customer exists & return result
			return false;
		} else {
			return true;
		}

	}
			//Adrian Kemmy A00199175
			// addPublicationsToDeliveryList - error handling
			public boolean tryCatchAddPublicationToDeliveryList(int customerId,
					int publicationId) {
						try{ // run processing method
							ExecuteAddPublicationToDeliveryList(customerId, publicationId); 
							return true;
						}catch(Exception error){
							return false;
						}
			}

			// Adrian Kemmy A00199175
			// addPublicationsToDeliveryList - processing
			private void ExecuteAddPublicationToDeliveryList(int customerId,
					int publicationId) throws SQLException {
				toExecute = "INSERT INTO customersPublications VALUES("+customerId+","+publicationId+");";
				statement.executeUpdate(toExecute);
				
			}
	public boolean tryCatchAddPublicationToDatabase(String publicationName,
			double publicationPrice) {
		try { // run processing method
			ExecuteAddPublicationToDatabase(publicationName, publicationPrice);
			return true;
		} catch (Exception error) {
			return false;
		}
	}

	// Adrian Kemmy A00199175
	// addPublicationsToDatabase - processing
	private void ExecuteAddPublicationToDatabase(String publicationName,
			double publicationPrice) throws SQLException {
		toExecute = "INSERT INTO Publication VALUES(" + null + ",'"
				+ publicationName + "','" + publicationPrice + "');";
		statement.executeUpdate(toExecute);

	}

	// Adrian Kemmy A00199175
	// removePublicationFromDatabase - error handling
	public boolean tryCatchRemovePublicationFromDatabase(int publicationId) {
		try { // run processing method
			ExecuteRemovePublicationFromDatabase(publicationId);
			return true;
		} catch (Exception error) {
			return false;
		}
	}

	// Adrian Kemmy A00199175
	// removePublicationsFromDatabase - processing
	private void ExecuteRemovePublicationFromDatabase(int publicationId)
			throws SQLException {
		toExecute = "DELETE FROM publication WHERE pub_id = " + publicationId
				+ ";";
		statement.executeUpdate(toExecute);

	}

	// Adrian Kemmy A00199175
	// updatePublicationToDatabase - error handling
	public boolean tryCatchUpdatePublicationToDatabase(int publicationId,
			String publicationName, double publicationCost) {
		try { // run processing method
			ExecuteUpdatePublicationToDatabase(publicationId, publicationName,
					publicationCost);
			return true;
		} catch (Exception error) {
			return false;
		}
	}

	// Adrian Kemmy A00199175
	// updatePublicationsToDatabase - processing
	private void ExecuteUpdatePublicationToDatabase(int publicationId,
			String publicationName, double publicationCost) throws SQLException {
		toExecute = "UPDATE publication SET pub_name = '" + publicationName
				+ "', cost = '" + publicationCost + "' WHERE pub_id = "
				+ publicationId + ";";
		statement.executeUpdate(toExecute);

	}
	
	//Vijay Patrick
	//method to get balance amount
	public float getBalance(int cust_id){
		float balance = 0;				
		try {		
			toExecute = "Select * from bills where cust_id = "  + cust_id + ";";
			resultSet = statement.executeQuery(toExecute);
			while(resultSet.next()){
				balance = resultSet.getFloat(3);
				System.out.println(balance);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return balance;
	}
	
	//Vijay Patrick
	//method to calculate the bill table
	public boolean calculateBill( String[] publications,int id){
		boolean updated = false;
		float price = 0.50f;
		try {			
			for(int i=0;i<publications.length;i++){
				toExecute = "select * from publication where pub_name = "  + publications[i] + ";";				
				resultSet = statement.executeQuery(toExecute);
				while(resultSet.next()){
					price = price+ resultSet.getFloat(1);
				}
			}
			System.out.println("Final Cost     "+price);			
			updated=updateBill(price,id);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return updated;
	}
	
	//Vijay Patrick
	//method to update the bill table
	private boolean updateBill(float price,int cust_id) {
		
		float initialAmount = 0;
		try{
			
			System.out.println(price+"      "+cust_id);
			toExecute = "select amount from bills where cust_id = "  + cust_id + ";";	
			//statement = connection.prepareStatement("select amount from bills where cust_id = ?");	
			
			resultSet = statement.executeQuery(toExecute);
			while(resultSet.next()){
				initialAmount = (resultSet.getFloat(1));
			}
			toExecute = "update bills set amount = "+ price + "where cust_id" + cust_id + ";";
			
			statement.executeUpdate(toExecute);
			return true;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	
	//Vijay Patrick
	//method to get data to print the bill table
	public PrintBill dataForFileBill(int id) {
				
		PrintBill printBill = new PrintBill();
		System.out.println(id);
		try{
					
			
			String toExecute = "SELECT table1.cust_id, table1.firstName, table1.lastName, table2.amount FROM customers AS table1, bills AS table2 WHERE "+
					"  table1.cust_id =? AND table1.cust_id = table2.cust_id ";
			
	
			resultSet = statement.executeQuery(toExecute);
			while(resultSet.next()){
				printBill = new PrintBill(resultSet.getInt(1), resultSet.getString(2)+"  "+resultSet.getString(3), resultSet.getFloat(4));
			}
			
			return printBill;
		}catch(Exception ex){
			ex.printStackTrace();
			return printBill;
		}
	}
	
	

} // end of DataBase class

