
package Code;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

// Author Declan A00195947
public class Window extends JFrame implements ActionListener {

	// declaration 
	// card layout
	JPanel cards;
	CardLayout cardlayout;

	// cards
	JPanel mainCard;
	JPanel registrationCard;
	JPanel loginCard;
	JPanel publicationsCard;
	JPanel paymentCard;
	JPanel custimzationCard;
	JPanel deliviresCard;
	JPanel miscCard;

	// menu bar
	JMenuBar bar;
	
	JMenu mainMenu;
	JMenuItem showMainCard;
	
	JMenu registrationMenu;
	JMenuItem showRegistrationCard;
	
	JMenu loginMenu;
	JMenuItem showLoginCard;
	
	JMenu publicationsMenu;
	JMenuItem showPublicationsCard;
	
	JMenu paymentMenu;
	JMenuItem showPaymentCard;
	
	JMenu custimzationMenu;
	JMenuItem showCustimzationCard;
	
	JMenu deliviresMenu;
	JMenuItem showDeliviresCard;
	
	JMenu miscMenu;
	JMenuItem showMiscCard;
	
	// registration card declaration
	// staff register
	JTextField emlployeeFirstNameField;
	JTextField emlployeeLastNameField;
	JTextField emlployeeAddressField;
	JTextField emlployeeContactNumberField;
	JPasswordField employeePasswordField;
	JButton staffRegister;
	
	//customer register
	JTextField firstNameField;
	JTextField lastNameField;
	JTextField houseNoField;
	JTextField streetField;
	JTextField locationField;
	JTextField townField;
	JTextField contactNoField;
	JButton registerCustomer;
	
	
	// adrian variables
	JTextField textField;
	

	public Window() {
		// content pane
		Container content = getContentPane();
		content.setLayout(new BorderLayout());

		GradientBackGround contentPanel = new GradientBackGround();
		contentPanel.setLayout(new FlowLayout());
		

		// registration card -- Author: Declan A00195947
		registrationCard = new JPanel();
		registrationCard.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));
		registrationCard.setLayout(new BorderLayout());
		
		// identify panel
		JPanel indentifyRegistrationCard = new JPanel();
		indentifyRegistrationCard.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JLabel labelRegistrationCard = new JLabel("Registration Interface");
		
		indentifyRegistrationCard.add(labelRegistrationCard);
		
		
		// staff panels
		JPanel staffRegistrationPanel = new JPanel();
		staffRegistrationPanel.setLayout(new BorderLayout());
		
		JPanel staffIdentifyPanel = new JPanel();
		staffIdentifyPanel.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JLabel labelStaffRegistrationPanel = new JLabel("Staff");
		
		staffIdentifyPanel.add(labelStaffRegistrationPanel);
		
		// staff content 
		JPanel staffPanelContent = new JPanel();
		staffPanelContent.setBorder(BorderFactory.createLoweredBevelBorder());
		JLabel employeeFirstName = new JLabel("FirstName: ");
		employeeFirstName.setBounds(25, 9, 81, 15);
		emlployeeFirstNameField = new JTextField("",10);
		emlployeeFirstNameField.setBounds(155, 7, 114, 19);
		JLabel employeeLastName = new JLabel("LastName: ");
		employeeLastName.setBounds(25, 40, 80, 15);
		emlployeeLastNameField = new JTextField("", 10);
		emlployeeLastNameField.setBounds(155, 38, 114, 19);
		JLabel emlployeeAddress = new JLabel("Address:");
		emlployeeAddress.setBounds(25, 71, 63, 15);
		emlployeeAddressField = new JTextField("", 10);
		emlployeeAddressField.setBounds(155, 69, 114, 19);
		JLabel emlployeeContactNumber = new JLabel("Contact No:");
		emlployeeContactNumber.setBounds(25, 102, 101, 15);
		emlployeeContactNumberField = new JTextField("",10);
		emlployeeContactNumberField.setBounds(155, 100, 114, 19);
		JLabel employeePassword = new JLabel("Password: ");
		employeePassword.setBounds(27, 132, 79, 15);
		employeePasswordField = new JPasswordField("",10);
		employeePasswordField.setBounds(155, 131, 114, 19);
		staffRegister = new JButton("Register");
		staffRegister.setBounds(25, 195, 94, 25);
		staffPanelContent.setLayout(null);
		
		staffPanelContent.add(employeeFirstName);
		staffPanelContent.add(emlployeeFirstNameField);
		staffPanelContent.add(employeeLastName);
		staffPanelContent.add(emlployeeLastNameField);
		staffPanelContent.add(emlployeeAddress);
		staffPanelContent.add(emlployeeAddressField);
		staffPanelContent.add(emlployeeContactNumber);
		staffPanelContent.add(emlployeeContactNumberField);
		staffPanelContent.add(employeePassword);
		staffPanelContent.add(employeePasswordField);
		staffPanelContent.add(staffRegister);
		
		// add to staff panel
		staffRegistrationPanel.add(staffIdentifyPanel, BorderLayout.NORTH);
		staffRegistrationPanel.add(staffPanelContent, BorderLayout.CENTER);
		
		
		// customer panels
		JPanel customerRegistrationPanel = new JPanel();
		customerRegistrationPanel.setLayout(new BorderLayout());
		
		JPanel customerIdentifyPanel = new JPanel();
		customerIdentifyPanel.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JLabel labelCustomerRegistrationPanel = new JLabel("Customer");
		
		customerIdentifyPanel.add(labelCustomerRegistrationPanel);
		
		// customer content 
		JPanel customerPanelContent = new JPanel();
		customerPanelContent.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JLabel firstNameLabel = new JLabel("FirstName:");
		firstNameLabel.setBounds(13, 12, 78, 15);
		firstNameField = new JTextField("", 10);
		firstNameField.setBounds(149, 10, 114, 19);
		JLabel lastNameLabel = new JLabel("LastName:");
		lastNameLabel.setBounds(13, 39, 77, 15);
		lastNameField = new JTextField("", 10);
		lastNameField.setBounds(149, 37, 114, 19);
		JLabel houseNoLabel = new JLabel("House No:");
		houseNoLabel.setBounds(13, 63, 78, 15);
		houseNoField = new JTextField("", 10);
		houseNoField.setBounds(149, 61, 114, 19);
		JLabel streetLabel = new JLabel("Street:");
		streetLabel.setBounds(13, 90, 78, 15);
		streetField = new JTextField("", 10);
		streetField.setBounds(149, 88, 114, 19);
		JLabel locationLabel = new JLabel("Location: ");
		locationLabel.setBounds(13, 117, 78, 15);
		locationField = new JTextField("", 10);
		locationField.setBounds(149, 115, 114, 19);
		JLabel townLabel = new JLabel("Town:");
		townLabel.setBounds(13, 144, 78, 15);
		townField = new JTextField("", 10);
		townField.setBounds(149, 142, 114, 19);
		JLabel contactNoLabel = new JLabel("Contact No:");
		contactNoLabel.setBounds(13, 171, 94, 15);
		contactNoField = new JTextField("", 10);
		contactNoField.setBounds(149, 169, 114, 19);
		registerCustomer = new JButton("Register");
		registerCustomer.setBounds(13, 198, 94, 25);
		customerPanelContent.setLayout(null);
		
		customerPanelContent.add(firstNameLabel);
		customerPanelContent.add(firstNameField);
		customerPanelContent.add(lastNameLabel);
		customerPanelContent.add(lastNameField);
		customerPanelContent.add(houseNoLabel);
		customerPanelContent.add(houseNoField);
		customerPanelContent.add(streetLabel);
		customerPanelContent.add(streetField);
		customerPanelContent.add(locationLabel);
		customerPanelContent.add(locationField);
		customerPanelContent.add(townLabel);
		customerPanelContent.add(townField);
		customerPanelContent.add(contactNoLabel);
		customerPanelContent.add(contactNoField);
		customerPanelContent.add(registerCustomer);
		
		
		// add to customer panel
		customerRegistrationPanel.add(customerIdentifyPanel, BorderLayout.NORTH);
		customerRegistrationPanel.add(customerPanelContent, BorderLayout.CENTER);
		
		// registration panel
		JPanel registrationPanel = new JPanel();
		registrationPanel.setLayout(new GridLayout(1,2));
		
		registrationPanel.add(staffRegistrationPanel);
		registrationPanel.add(customerRegistrationPanel);
		
		// add to registration card
		
		registrationCard.add(indentifyRegistrationCard, BorderLayout.NORTH);
		registrationCard.add(registrationPanel, BorderLayout.CENTER);

		// login card -- Author: Jenna
		loginCard = new JPanel();
		loginCard.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));
		loginCard.setLayout(new BorderLayout());
		
		JPanel indentifyLoginCard = new JPanel();
		indentifyLoginCard.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JLabel labelLoginCard = new JLabel("Login Interface");
		
		indentifyLoginCard.add(labelLoginCard);
		
		// add to login card
		
		loginCard.add(indentifyLoginCard, BorderLayout.NORTH);

		// publications card -- Author: Paul
		publicationsCard = new JPanel();
		publicationsCard.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));
		publicationsCard.setLayout(new BorderLayout());
		
		JPanel indentifyPublicationsCard = new JPanel();
		indentifyPublicationsCard.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JLabel labelPublicationsCard = new JLabel("Publications Interface");
		
		indentifyPublicationsCard.add(labelPublicationsCard);
		
		// add to publications card
		
		publicationsCard.add(indentifyPublicationsCard, BorderLayout.NORTH);

		// payment card -- Author: Adrian
				paymentCard = new JPanel();
				paymentCard.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));
				paymentCard.setLayout(null);
				
				JPanel indentifyPaymentCard = new JPanel();
				indentifyPaymentCard.setBounds(2, 2, 596, 28);
				indentifyPaymentCard.setBorder(BorderFactory.createLoweredBevelBorder());
				
				JLabel labelPaymentCard = new JLabel("Payment Interface");
				
				indentifyPaymentCard.add(labelPaymentCard);
				
				// add to payment card
				// add to payment card
				
						
						JComboBox comboBox = new JComboBox();
						comboBox.setToolTipText("");
						comboBox.setMaximumRowCount(500);
						comboBox.setBounds(274, 56, 251, 43);
					
						
						JButton btnAddPayment = new JButton("Add Payment");
						btnAddPayment.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								
							}
						});
						btnAddPayment.setBounds(167, 176, 185, 43);
					
						
						textField = new JTextField();
						textField.setBounds(284, 110, 145, 36);
						paymentCard.add(textField);
						textField.setColumns(10);
						
						JLabel paymentValueLabel = new JLabel("Enter Payment Amount Here:");
						paymentValueLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
						paymentValueLabel.setBounds(76, 116, 167, 30);
						
						
						JLabel paymentCustomerSelect = new JLabel("Choose a Customer:");
						paymentCustomerSelect.setFont(new Font("Tahoma", Font.PLAIN, 13));
						paymentCustomerSelect.setBounds(76, 70, 167, 14);
						
						paymentCard.add(btnAddPayment);
						paymentCard.add(comboBox);
						paymentCard.add(paymentValueLabel);
						paymentCard.add(paymentCustomerSelect);
				
				
				
				
				paymentCard.add(indentifyPaymentCard);

		// custimzation card -- Author: Adrian
		custimzationCard = new JPanel();
		custimzationCard.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));
		custimzationCard.setLayout(new BorderLayout());
		
		JPanel indentifyCustimzationCard = new JPanel();
		indentifyCustimzationCard.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JLabel labelCustimzationCard = new JLabel("Custimzation Interface");
		
		indentifyCustimzationCard.add(labelCustimzationCard);
		
		// add to payment card
		
		custimzationCard.add(indentifyCustimzationCard, BorderLayout.NORTH);

		// delivires card -- Author: Jenna
		deliviresCard = new JPanel();
		deliviresCard.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));
		deliviresCard.setLayout(new BorderLayout());
		
		JPanel indentifyDeliviresCard = new JPanel();
		indentifyDeliviresCard.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JLabel labelDeliviresCard = new JLabel("Delivires Interface");
		
		indentifyDeliviresCard.add(labelDeliviresCard);
		
		// add to payment card
		
		deliviresCard.add(indentifyDeliviresCard, BorderLayout.NORTH);

		// misc card -- Author: Paul
		miscCard = new JPanel();
		miscCard.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));
		miscCard.setLayout(new BorderLayout());
		
		JPanel indentifyMiscCard = new JPanel();
		indentifyMiscCard.setBorder(BorderFactory.createLoweredBevelBorder());
		
		JLabel labelMiscCard = new JLabel("Misc Interface");
		
		indentifyMiscCard.add(labelMiscCard);
		
		// add to payment card
		
		miscCard.add(indentifyMiscCard, BorderLayout.NORTH);

		// card Collection
		cards = new JPanel(new CardLayout());
		cards.setPreferredSize(new Dimension(600, 320));

		cards.add(loginCard, "loginCard");
		cards.add(registrationCard, "registrationCard");
		cards.add(publicationsCard, "publicationsCard");
		cards.add(paymentCard, "paymentCard");
		cards.add(custimzationCard, "custimzationCard");
		cards.add(deliviresCard, "deliviresCard");
		cards.add(miscCard, "miscCard");

		// add to content pane
		contentPanel.add(cards);

		content.add(contentPanel, BorderLayout.CENTER);

		cardlayout = (CardLayout) (cards.getLayout());
		cardlayout.show(cards, "loginCard");

		// menuBar

		bar = new JMenuBar();

		loginMenu = new JMenu("Login");
		showLoginCard = new JMenuItem("loginCard");
		registrationMenu = new JMenu("Registration");
		showRegistrationCard = new JMenuItem("reistrationCard");
		publicationsMenu = new JMenu("Publications");
		showPublicationsCard = new JMenuItem("publicationsCard");
		paymentMenu = new JMenu("Payment");
		showPaymentCard = new JMenuItem("paymentCard");
		custimzationMenu = new JMenu("Custimzation");
		showCustimzationCard = new JMenuItem("custimzationCard");
		deliviresMenu = new JMenu("Delivires");
		showDeliviresCard = new JMenuItem("deliviresCard");
		miscMenu = new JMenu("Misc");
		showMiscCard = new JMenuItem("miscCard");
		
		loginMenu.add(showLoginCard);
		registrationMenu.add(showRegistrationCard);
		publicationsMenu.add(showPublicationsCard);
		paymentMenu.add(showPaymentCard);
		custimzationMenu.add(showCustimzationCard);
		deliviresMenu.add(showDeliviresCard);
		miscMenu.add(showMiscCard);

		bar.add(loginMenu);
		bar.add(registrationMenu);
		bar.add(publicationsMenu);
		bar.add(paymentMenu);
		bar.add(custimzationMenu);
		bar.add(deliviresMenu);
		bar.add(miscMenu);

		// add action listeners
		// menuBar action listeners
		showLoginCard.addActionListener(this);
		showRegistrationCard.addActionListener(this);
		showPublicationsCard.addActionListener(this);
		showPaymentCard.addActionListener(this);
		showCustimzationCard.addActionListener(this);
		showDeliviresCard.addActionListener(this);
		showMiscCard.addActionListener(this);
		
		// register card action listeners
		staffRegister.addActionListener(this); 
		registerCustomer.addActionListener(this);

		// frame parametres
		setTitle("Agile Newsagent System");
		setMinimumSize(new Dimension(650, 380));
		setPreferredSize(new Dimension(650, 380));
		setMaximumSize(new Dimension(650, 380));
		setResizable(false);
		setJMenuBar(bar);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);

	}

	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();
		
		if(target == showLoginCard){
			cardlayout.show(cards, "loginCard");
		}
		if(target == showRegistrationCard){
			cardlayout.show(cards, "registrationCard");
		}
		if(target == showPublicationsCard){
			cardlayout.show(cards, "publicationsCard");
		}
		if(target == showPaymentCard){
			cardlayout.show(cards, "paymentCard");
		}
		if(target == showCustimzationCard){
			cardlayout.show(cards, "custimzationCard");
		}
		if(target == showDeliviresCard){
			cardlayout.show(cards, "deliviresCard");
		}
		if(target == showMiscCard){
			cardlayout.show(cards, "miscCard");
		}
		
		if(target == staffRegister){
			
		}
		
		if(target == registerCustomer){
			try {
				Customer.registerCustomer(firstNameField.getText(), lastNameField.getText(),townField.getText(),streetField.getText(),locationField.getText(), Integer.parseInt(houseNoField.getText()),contactNoField.getText());
				firstNameField.setText(""); 
				lastNameField.setText("");
				townField.setText("");
				streetField.setText("");
				locationField.setText("");
				houseNoField.setText("");
				contactNoField.setText("");
			} catch (Exception error) {
				
			}
		}

	}

	public static void main(String[] args) {
		Window newsAgentGui = new Window();
	}

}
