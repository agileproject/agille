package tests;

import Code.Bills;
import Code.DataBase;

import junit.framework.TestCase;

public class BillsTest extends TestCase {
	DataBase db = DataBase.getInstance();
	Bills bills = new Bills();
	// Test No: 1
	// Objective: To get the publication price from db
	// Inputs:db
	// Expected Output: value
	public void testGetPublicationsPrice001() {

		Bills bills = new Bills();

		try {
			assertEquals(true, bills.getPublicationsPrice());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Test No: 2
	// Objective: To get the publication price from db
	// Inputs:db
	// Expected Output:value

	public void testGetPublicationsPrice002() {

		Bills bills = new Bills();

		try {
			assertEquals(false, bills.getPublicationsPrice());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	// Test No: 1
	// Objective: To get the delivery price from db
	// Inputs:db
	// Expected Output: value

	public void testGetDeliveryPrice001() {
		Bills bills = new Bills();
		try {
			assertEquals(true, bills.getDeliveryPrice());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	// Test No: 2
	// Objective: To get the delivery price from db
	// Inputs:db
	// Expected Output: value

	public void testGetDeliveryPrice002() {
		Bills bills = new Bills();
		try {
			assertEquals(false, bills.getDeliveryPrice());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	// Test No: 1
	// Objective: To get the CustomerBalance price from db
	// Inputs:db
	// Expected Output: balance

	public void testGetCustomerBalance001() {
		Bills bills = new Bills();
		try {
			assertEquals(true, bills.getCustomerBalance());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	// Test No: 2
	// Objective: To get the CustomerBalance price from db
	// Inputs:db
	// Expected Output: balance

	public void testGetCustomerBalance002() {
		Bills bills = new Bills();
		try {
			assertEquals(false, bills.getCustomerBalance());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	// Test No: 1
	// Objective: To get the CustomerBalance price from db
	// Inputs:db
	// Expected Output: balance

	public void testCalculateBill01() {

		Bills bills = new Bills();
		try {
			assertEquals(true, bills.getCustomerBalance());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	// Test No: 2
	// Objective: To get the CustomerBalance price from db
	// Inputs:db
	// Expected Output: balance

	public void testCalculateBill02() {
		Bills bills = new Bills();
		try {
			assertEquals(false, bills.getCustomerBalance());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
