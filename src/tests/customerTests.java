package tests;

import junit.framework.TestCase;
import Code.Customer;
import Code.DataBase;
import Code.Publications;
import Handler.ExceptionHandler;

public class customerTests extends TestCase {

	DataBase databaseInstance = DataBase.getInstance();

	// Patrick Murray A00202026
	// Test 1
	// Obj = is customer in the database
	// Input: first & last Name(customer is in database)
	// Expected output: true
	public void testupdatePersonalInformation1() {
		try {
			assertEquals(true,
					databaseInstance.searchCustomer("Vijay", "Patrick"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 2
	// Obj = is customer in the database
	// Input: first & last Name(customer is not in database)
	// Expected output: false
	public void testupdatePersonalInformation2() {
		try {
			assertEquals(false, databaseInstance.searchCustomer("Joe", "Smith"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 3
	// Obj = if empty parameters are passed
	// Input: null null
	// Expected output: false
	public void testupdatePersonalInformation3() {
		try {
			assertEquals(false, databaseInstance.searchCustomer(null, null));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 4
	// Obj = if first name and last name arent from same user
	// Input:
	// Expected output:
	public void testupdatePersonalInformation4() {
		try {
			assertEquals(false,
					databaseInstance.searchCustomer("Vijay", "Murray"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 5
	// Obj = if only first name is provided
	// Input: String Patrick, null
	// Expected output: false
	public void testupdatePersonalInformation5() {
		try {
			assertEquals(false, databaseInstance.searchCustomer("Vijay", null));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 6
	// Obj = if only last name is provided
	// Input: null String Murray
	// Expected output: false
	public void testupdatePersonalInformation6() {
		try {
			assertEquals(false, databaseInstance.searchCustomer(null, "Murray"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Declan Farrell A00195947
	// RegisterCustomer Test 1
	// Obj = test customer Register with correct Info (all acceptance criteria met)
	// Input: firstname = Declan, lastname = farrell, town = someTown, Street =
	// someStreet location = "someLocation",houseNo = 42,ContactNumber = 081 1111111 
	// ,status = true;
	// Expected output:true

	public void testRegisterCustomer1() {
		try {
			assertTrue(Customer.registerCustomer("Declan", "Farrell",
					"someTown", "someStreet", "someLocation", 42,
					"081 111 1111"));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("registerCustomerError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// RegisterCustomer Test 2
	// Obj = test customer Register with incorrect info (no acceptance criteria
	// met)
	// Input: firstname = "", lastname = "", town = "", Street = ""
	// location = "",houseNo = 0, status = false;
	// Expected output: false

	public void testRegisterCustomer2() {
		try {
			assertFalse(Customer.registerCustomer("", "", "", "", "", 0, ""));
			fail("should not get here");
		} catch (Handlers.ExceptionHandler error) {
			assertSame("registerCustomerError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// RegisterCustomer Test 3
	// Obj = test customer register verify correct contact number (08)
	// Input: firstname = Declan, lastname = farrell, town = someTown, Street =
	// someStreet location = "someLocation",houseNo = 52,ContactNumber = 084 444
	// 4444 ,status = true;
	// Expected output: true
	public void testRegisterCustomer3() {
		try {
			assertTrue(Customer.registerCustomer("Declan", "Farrell",
					"someTown", "someStreet", "someLocation", 52, "084444444"));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("registerCustomerError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// RegisterCustomer Test 4
	// Obj = test customer register verify correct contact number (090)
	// Input: firstname = Declan, lastname = farrell, town = someTown, Street =
	// someStreet location = "someLocation",houseNo = 14,ContactNumber =
	// 0906444444,status = true;
	// Expected output: true
	public void testRegisterCustomer4() {
		try {
			assertTrue(Customer.registerCustomer("Declan", "Farrell",
					"someTown", "someStreet", "someLocation", 14, "0906444444"));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("registerCustomerError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// RegisterCustomer Test 5
	// Obj = test customer register verify incorrect contact number fails
	// Input: firstname = Declan, lastname = farrell, town = someTown, Street =
	// someStreet location = "someLocation",houseNo = 19,ContactNumber =
	// 3333333333,status = true;
	// Expected output: true
	public void testRegisterCustomer5() {
		try {
			assertFalse(Customer.registerCustomer("Declan", "Farrell",
					"someTown", "someStreet", "someLocation", 19, "3333333333"));
			fail("should not get here");
		} catch (Handlers.ExceptionHandler error) {
			assertSame("registerCustomerError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// RegisterCustomer Test 6
	// Obj = test customer register passes with houseNumber 0
	// Input: firstname = Declan, lastname = farrell, town = someTown, Street =
	// someStreet location = "someLocation",houseNo = 0,ContactNumber =
	// 0906444444,status = true;
	// Expected output: true
	public void testRegisterCustomer6() {
		try {
			assertTrue(Customer.registerCustomer("Declan", "Farrell",
					"someTown", "someStreet", "someLocation", 0, "0906444444"));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("registerCustomerError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// RegisterCustomer Test 7
	// Obj = test customer register passes with houseNumber 1
	// Input: firstname = Declan, lastname = farrell, town = someTown, Street =
	// someStreet location = "someLocation",houseNo = 1,ContactNumber =
	// 0906444444,status = true;
	// Expected output: true
	public void testRegisterCustomer7() {
		try {
			assertTrue(Customer.registerCustomer("Declan", "Farrell",
					"someTown", "someStreet", "someLocation", 1, "0906444444"));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("registerCustomerError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// RegisterCustomer Test 8
	// Obj = test customer register fails with houseNumber -1
	// Input: firstname = Declan, lastname = farrell, town = someTown, Street =
	// someStreet location = "someLocation",houseNo = -1,ContactNumber =
	// 0906444444,status = true;
	// Expected output: false (exception thrown)
	public void testRegisterCustomer8() {
		try {
			assertFalse(Customer.registerCustomer("Declan", "Farrell",
					"someTown", "someStreet", "someLocation", -1, "0906444444"));
			fail("should not get here");
		} catch (Handlers.ExceptionHandler error) {
			assertSame("registerCustomerError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// viewCustomerDetails test 1
	// obj = verify view details returns customer details
	// inputs: customer id = 5
	// Expected output = pass
	public void testViewCustomerDetails1() {
		try {
			assertNotNull(Customer.viewCustomerDetails(5));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("viewCustomerDetailsError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// viewCustomerDetails test 2
	// obj = verify view details fails to return unknown customer details
	// inputs: customer id = 10000000
	// Expected output = pass (
	public void testViewCustomerDetails2() {
		try {
			assertNull(Customer.viewCustomerDetails(10000000));
			fail("should not get here");
		} catch (Handlers.ExceptionHandler error) {
			assertSame("viewCustomerDetailsError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// viewCustomerDetails test 3
	// obj = verify view details fails when invalid id entered
	// inputs: customer id = -100
	// Expected output = pass (exception thrown)
	public void testViewCustomerDetails3() {
		try {
			assertNull(Customer.viewCustomerDetails(-100));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("viewCustomerDetailsError", error.getMessage());
		}
	}


		// Adrian Kemmy A00199175
		// addPublicationsToDeliveryList test 1
		// obj = verify that true returned when correct customerId and publicationId
		// are passed
		// inputs = customerId = 4, publicationId = 1
		// expected output = true
		public void testAddPublicationToDeliveryList1() {
			try {
				assertTrue(Customer.addPublicationToDeliveryList(1, 1));
			} catch (Handlers.ExceptionHandler error) {
				assertSame("addPublicationToDeliveryListError",error.getMessage());
			}
		}

		// Adrian Kemmy A00199175
		// addPublicationsToDeliveryList test 2
		// obj = verify that false returned when incorrect customerId and
		// publicationId are passed
		// inputs = customerId = -1000, publicationId = -100
		// expected output = pass (Exception thrown)
		public void testAddPublicationToDeliveryList2() {
			try {
				assertFalse(Customer.addPublicationToDeliveryList(-1000, -100));
				fail("should not get here");
			} catch (Handlers.ExceptionHandler error) {
				assertSame("addPublicationToDeliveryListError",error.getMessage());
			}
		}

		// Adrian Kemmy A00199175
		// addPublicationsToDeliveryList test 3
		// obj = verify that false returned when unknown customerId and
		// publicationId are passed
		// inputs = customerId = 100000, publicationId = 4567
		// expected output = pass (Exception thrown)
		public void testAddPublicationToDeliveryList3() {
			try {
				assertFalse(Customer
						.addPublicationToDeliveryList(100000, 4567));
				fail("should not get here");
			} catch (Handlers.ExceptionHandler error) {
				assertSame("addPublicationToDeliveryListError",
						error.getMessage());
			}
		}

		// Adrian Kemmy A00199175
		// addPublicationsToDeliveryList test 4
		// obj = verify that false returned when correct customerId and incorrect
		// publicationId are passed
		// inputs = customerId = 8, publicationId = -1000
		// expected outputs = pass (exception thrown)
		public void testaddPublicationToDeliveryList4() {
			try {
				assertFalse(Customer.addPublicationToDeliveryList(8, -1000));
				fail("should not get here");
			} catch (Handlers.ExceptionHandler error) {
				assertSame("addPublicationToDeliveryListError",
						error.getMessage());
			}
		}

		// Adrian Kemmy A00199175
		// addPublicationsToDeliveryList test 5
		// obj = verify that false returned when incorrect customerId and correct
		// publicationId are passed
		// inputs = customerId = 10000000, publicationId = 4
		// expected outputs = pass (exception thrown)
		public void testaddPublicationToDeliveryList5() {
			try {
				assertFalse(Customer.addPublicationToDeliveryList(10000000, 4));
				fail("should not get here");
			} catch (Handlers.ExceptionHandler error) {
				assertSame("addPublicationToDeliveryListError",
						error.getMessage());
			}
		}
		// Adrian Kemmy A00199175
			// addPublicationToDatabase test 1
			// obj = verify that true returned when correct publicationName, and publicationCost
			// are passed
			// inputs =  publicationName = "Daily Mail", publicationCost = 1.10
			// expected output = true
			public void testAddPublicationToDatabase1() {
				try {
					assertTrue(Publications.addPublicationToDatabase("Daily Mail",1.10));
				} catch (Handlers.ExceptionHandler error) {
					assertSame("addPublicationToDatabaseError",error.getMessage());
				}
			}

			
			// Adrian Kemmy A00199175
			// addPublicationToDatabase test 2
			// obj = verify that false returned when out of bounds publicationPrice is passed
			// inputs = publicationName = "Daily Mail", publicationPrice = -1.10
			// expected output = pass (Exception thrown)
			public void testAddPublicationToDatabase2() {
				try {
					assertFalse(Publications.addPublicationToDatabase("Daily Mail",-1.10));
					fail("should not get here");
				} catch (Handlers.ExceptionHandler error) {
					assertSame("addPublicationToDatabaseError",
							error.getMessage());
				}
			}
			// Adrian Kemmy A00199175
					// removePublicationFromDatabase test 1
					// obj = verify that true returned when correct publicationId is passed
					// inputs =  publicationId = 2
					// expected output = true
					public void testRemovePublicationFromDatabase1() {
						try {
							assertTrue(Publications.removePublicationFromDatabase(2));
						} catch (Handlers.ExceptionHandler error) {
							assertSame("removePublicationFromDatabaseError",error.getMessage());
						}
					}

					
					// Adrian Kemmy A00199175
					// removePublicationFromDatabase test 2
					// obj = verify that false returned when incorrect publicationId is passed
					// inputs =  publicationId = -2
					// expected output = pass (Exception thrown)
					public void testRemovePublicationFromDatabase2() {
						try {
							assertFalse(Publications.removePublicationFromDatabase(-2));
							fail("should not get here");
						} catch (Handlers.ExceptionHandler error) {
							assertSame("removePublicationFromDatabaseError",
									error.getMessage());
						}
					}
					// Adrian Kemmy A00199175
					// updatePublicationToDatabase test 1
					// obj = verify that true returned when correct publicationId, publicationName, and publicationCost
					// are passed
					// inputs =  publicationId = 2 publicationName = "Daily Mail Test", publicationCost = 1.10
					// expected output = true
					public void testUpdatePublicationToDatabase1() {
						try {
							assertTrue(Publications.updatePublication(2, "Daily Mail Test", 1.10));
						} catch (Handlers.ExceptionHandler error) {
							assertSame("updatePublicationError",error.getMessage());
						}
					}

					
					// Adrian Kemmy A00199175
					// addPublicationToDatabase test 2
					// obj = verify that false returned when out of bounds publicationId is passed
					// inputs = publicationName = "Daily Mail", publicationPrice = -1.10
					// expected output = pass (Exception thrown)
					public void testUpdatePublicationToDatabase2() {
						try {
							assertFalse(Publications.updatePublication(-2, "Daily Mail Test", 1.10));
							fail("should not get here");
						} catch (Handlers.ExceptionHandler error) {
							assertSame("updatePublicationError",error.getMessage());
						}
					}
					
					// Adrian Kemmy A00199175
					// addPublicationToDatabase test 3
					// obj = verify that false returned when out of bounds publicationPrice is passed
					// inputs = publicationName = "Daily Mail", publicationPrice = -1.10
					// expected output = pass (Exception thrown)
					public void testUpdatePublicationToDatabase3() {
						try {
							assertFalse(Publications.updatePublication(2, "Daily Mail Test", -1.10));
							fail("should not get here");
						} catch (Handlers.ExceptionHandler error) {
							assertSame("updatePublicationError",error.getMessage());
						}
					}

					// Adrian Kemmy A00199175
					// addPublicationToDatabase test 4
					// obj = verify that false returned when out of bounds publicationId and publicationPrice are passed
					// inputs = publicationName = "Daily Mail", publicationPrice = -1.10
					// expected output = pass (Exception thrown)
					public void testUpdatePublicationToDatabase4() {
						try {
							assertFalse(Publications.updatePublication(-2, "Daily Mail Test", -1.10));
							fail("should not get here");
						} catch (Handlers.ExceptionHandler error) {
							assertSame("updatePublicationError",error.getMessage());
						}
					}

			

	// Declan Farrell A00195947
	// suspendService test 1
	// obj = verify suspendService passes when valid id is entered
	// inputs id = 6
	// expected output = pass
	public void testSuspendService1() {
		try {
			assertTrue(Customer.suspendCustomerService(6));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("suspendError", error.getMessage());
		}
	}

	// Declan Farrelll A00195947
	// suspendService test 2
	// obj = verify suspendService throws exception when invalid id is entered
	// inputs id = -1000
	// expected output = pass (exception thrown)
	public void testSuspendService2() {
		try {
			assertTrue(Customer.suspendCustomerService(-1000));
			fail("should not get here");
		} catch (Handlers.ExceptionHandler error) {
			assertSame("suspendError", error.getMessage());
		}
	}

	// Declan Farrelll A00195947
	// suspendService test 3
	// obj = verify suspendService throws exception when valid id is entered
	// (but customer does not exist)
	// inputs id = 10000000
	// expected output = pass (exception thrown)
	public void testSuspendService3() {
		try {
			assertTrue(Customer.suspendCustomerService(10000000));
			fail("should not get here");
		} catch (Handlers.ExceptionHandler error) {
			assertSame("suspendError", error.getMessage());
		}
	}

	// Declan Farrelll A00195947
	// resumeService test 1
	// obj = verify resumeService passes when valid id is entered
	// inputs id = 2
	// expected output = pass
	public void testResumeService1() {
		try {
			assertTrue(Customer.resumeCustomerService(2));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("resumeError", error.getMessage());
		}
	}

	// Declan Farrelll A00195947
	// resumeService test 2
	// obj = verify resumeService throws exception when invalid id is entered
	// inputs id = -4397
	// expected output = pass (exception thrown)
	public void testResumeService2() {
		try {
			assertTrue(Customer.resumeCustomerService(-4397));
			fail("should not get here");
		} catch (Handlers.ExceptionHandler error) {
			assertSame("resumeError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// resumeService test 3
	// obj = verify resumeService throws exception when valid id is entered (but
	// customer does not exist)
	// inputs id = 456930
	// expected output = pass (exception thrown)
	public void testResumeService3() {
		try {
			assertTrue(Customer.resumeCustomerService(456930));
			fail("should not get here");
		} catch (Handlers.ExceptionHandler error) {
			assertSame("resumeError", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// checkCustomerExists test 1
	// obj = verify true returned when valid customerId is entered
	// inputs customerId = 2
	// expected output = true
	public void testCheckCustomerExists1() {
		try {
			assertTrue(Customer.checkCustomerExists(2));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("failedToFindCustomer", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// checkCustomerExists test 2
	// obj = verify false returned when invalid customerId is entered
	// inputs customerId = 1000293489
	// expected output = false (pass)
	public void testCheckCustomerExists2() {
		try {
			assertFalse(Customer.checkCustomerExists(1000293489));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("failedToFindCustomer", error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// removePublicationsFromDeliveryList test 1
	// obj = verify that true returned when correct customerId and publicationId
	// are passed
	// inputs = customerId = 4, publicationId = 1
	// expected output = true
	public void testRemovePublicationsFromDeliveryList1() throws Handlers.ExceptionHandler, ExceptionHandler {
		try {
			assertTrue(Customer.removePublicationFromDeliveryList(4, 1));
		} catch (Handlers.ExceptionHandler error) {
			assertSame("removePublicationsFromDeliveryListError",
					error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// removePublicationsFromDeliveryList test 2
	// obj = verify that false returned when incorrect customerId and
	// publicationId are passed
	// inputs = customerId = -1000, publicationId = -100
	// expected output = pass (Exception thrown)
	public void testRemovePublicationsFromDeliveryList2() throws Handlers.ExceptionHandler {
		try {
			assertFalse(Customer.removePublicationFromDeliveryList(-1000, -100));
			fail("should not get here");
		} catch (Handler.ExceptionHandler error) {
			assertSame("removePublicationsFromDeliveryListError",
					error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// removePublicationsFromDeliveryList test 3
	// obj = verify that false returned when unknown customerId and
	// publicationId are passed
	// inputs = customerId = 100000, publicationId = 4567
	// expected output = pass (Exception thrown)
	public void testRemovePublicationsFromDeliveryList3() throws Handlers.ExceptionHandler {
		try {
			assertFalse(Customer
					.removePublicationFromDeliveryList(100000, 4567));
			fail("should not get here");
		} catch (Handler.ExceptionHandler error) {
			assertSame("removePublicationsFromDeliveryListError",
					error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// removePublicationsFromDeliveryList test 4
	// obj = verify that false returned when correct customerId and incorrect
	// publicationId are passed
	// inputs = customerId = 8, publicationId = -1000
	// expected outputs = pass (exception thrown)
	public void testRemovePublicationsFromDeliveryList4() throws Handlers.ExceptionHandler {
		try {
			assertFalse(Customer.removePublicationFromDeliveryList(8, -1000));
			fail("should not get here");
		} catch (Handler.ExceptionHandler error) {
			assertSame("removePublicationsFromDeliveryListError",
					error.getMessage());
		}
	}

	// Declan Farrell A00195947
	// removePublicationFromDeliveryList test 5
	// obj = verify that false returned when incorrect customerId and correct
	// publicationId are passed
	// inputs = customerId = 10000000, publicationId = 4
	// expected outputs = pass (exception thrown)
	public void testRemovePublicationsFromDeliveryList5() throws Handlers.ExceptionHandler {
		try {
			assertFalse(Customer.removePublicationFromDeliveryList(10000000, 4));
			fail("should not get here");
		} catch (Handler.ExceptionHandler error) {
			assertSame("removePublicationsFromDeliveryListError",
					error.getMessage());
		}
	}

}