package tests;

import junit.framework.TestCase;
import Code.DataBase;

public class databaseTests extends TestCase {

	DataBase db = DataBase.getInstance();

	// Patrick Murray A00202026
	// Test 1
	// Obj = test if passed parameter is in the database
	// Input: String newspaper
	// Expected output: true

	public void testsearchPublications1() {
		try {
			assertEquals(true, db.searchPublications("Irish Times"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 2
	// Obj = test if no parameter is passed to method
	// Input: String null
	// Expected output: false

	public void testsearchPublications2() {
		try {
			assertEquals(false, db.searchPublications(null));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 3
	// Obj = test search by publication ID
	// Input: int 1
	// Expected output: true

	public void testsearchPublications3() {
		try {
			assertEquals(true, db.searchPublications(1));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 4
	// Obj = test if negative number is passed to method
	// Input: int -2
	// Expected output: false

	public void testsearchPublications4() {
		try {
			assertEquals(false, db.searchPublications(-2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 5
	// Obj = test if publication is passed that is not in the database
	// Input: int The Star
	// Expected output: false

	public void testsearchPublications5() {
		try {
			assertEquals(false, db.searchPublications("The Star"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Patrick Murray A00202026
	// Test 6
	// Obj = test if publication ID is passed that is not in the Database
	// Input: int 55
	// Expected output: false

	public void testsearchPublications6() {
		try {
			assertEquals(false, db.searchPublications(55));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
