package tests;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.SwingConstants;

public class SetTime {

	private JFrame frmTime;
	private JLabel label1;
	private JLabel label2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SetTime window = new SetTime();
					window.frmTime.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SetTime() {
		initialize();
		clock();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frmTime = new JFrame();
		frmTime.setTitle("Time");
		frmTime.setBounds(100, 100, 450, 300);
		frmTime.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTime.getContentPane().setLayout(null);

		label1 = new JLabel("New label");
		label1.setHorizontalAlignment(SwingConstants.CENTER);
		label1.setFont(new Font("Tahoma", Font.BOLD, 18));
		label1.setBounds(36, 30, 374, 56);
		frmTime.getContentPane().add(label1);

		label2 = new JLabel("New label");
		label2.setFont(new Font("Tahoma", Font.BOLD, 18));
		label2.setHorizontalAlignment(SwingConstants.CENTER);
		label2.setBounds(36, 97, 374, 61);
		frmTime.getContentPane().add(label2);
	}

	public void clock() {
		Thread clock = new Thread() {
			public void run() {
				try {
					for (;;) {
						Calendar cal = new GregorianCalendar();
						int day = cal.get(Calendar.DAY_OF_MONTH);
						int month = cal.get(Calendar.MONTH);
						int year = cal.get(Calendar.YEAR);

						int second = cal.get(Calendar.SECOND);
						int minute = cal.get(Calendar.MINUTE);
						int hour = cal.get(Calendar.HOUR);

						label1.setText("Date " + day + ":" + month + ":" + year);
						label2.setText("Time " + hour + ":" + minute + ":" + second);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		};
		clock.start();
	}
}
