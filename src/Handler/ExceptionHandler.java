package Handler;

public class ExceptionHandler extends Exception{
	String message;

	public ExceptionHandler(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
